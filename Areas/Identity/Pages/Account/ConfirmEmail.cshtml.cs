﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.WebUtilities;
using StonksTracker.Data;
using StonksTracker.Models.Stocks;

namespace StonksTracker.Areas.Identity.Pages.Account
{
    [AllowAnonymous]
    public class ConfirmEmailModel : PageModel
    {
        private readonly UserManager<IdentityUser> _userManager;
        private readonly DepotContext _context;



        public ConfirmEmailModel(UserManager<IdentityUser> userManager, DepotContext context)
        {
            _userManager = userManager;
            _context = context;
        }

        [TempData]
        public string StatusMessage { get; set; }

        public async Task<IActionResult> OnGetAsync(string userId, string code)
        {
            if (userId == null || code == null)
            {
                return RedirectToPage("/Index");
            }

            var user = await _userManager.FindByIdAsync(userId);
            if (user == null)
            {
                return NotFound($"Unable to load user with ID '{userId}'.");
            }

            code = Encoding.UTF8.GetString(WebEncoders.Base64UrlDecode(code));
            var result = await _userManager.ConfirmEmailAsync(user, code);
            StatusMessage = result.Succeeded ? "E-Mail erfolgreich bestätigt" : "E-mail konnte nicht bestätigt werden.";
            if (result.Succeeded)
            {
                // Create the User in our Depot Database, they are completely separated.
                UserModel newUser = new UserModel { Guid = user.Id};
                _context.UserModel.Add(newUser);
                await _context.SaveChangesAsync();
            }
            return Page();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using StonksTracker.Models;
using StonksTracker.Models.Sites;
using StonksTracker.Services.SendGrid;

namespace StonksTracker.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly EmailSender EmailSender = new EmailSender();

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        public IActionResult Impressum()
        {
            return View();
        }

        public IActionResult Kontakt()
        {
            return View();
        }

        [HttpPost, ValidateAntiForgeryToken]
        public async Task<IActionResult> Kontakt(KontaktModel model)
        {
            if (!TryValidateModel(model)) return LocalRedirect("~/Home/Error");

            // Request seems good, send an Email to the developer
            await EmailSender.SendEmailAsync(model.email, "Kontaktanfrage - StonksTracker",
                        $"Hier Ihre Anfrage über das Kontaktformular auf <a href='https://stonkstracker.azurewebsites.net'>StonksTracker</a>:" +
                        $"{model.message}");





            return LocalRedirect("~/Home/KontaktErfolgreich"); ;
        }

        public IActionResult KontaktErfolgreich()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}

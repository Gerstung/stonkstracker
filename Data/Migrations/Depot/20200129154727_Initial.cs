﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace StonksTracker.Data.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Stock",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Wkn = table.Column<string>(nullable: true),
                    Isin = table.Column<string>(nullable: true),
                    Type = table.Column<int>(nullable: true),
                    Currency = table.Column<int>(nullable: true),
                    Price = table.Column<float>(nullable: false),
                    LastUpdate = table.Column<DateTime>(nullable: false),
                    StockOpen = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Stock", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "UserModel",
                columns: table => new
                {
                    Guid = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserModel", x => x.Guid);
                });

            migrationBuilder.CreateTable(
                name: "Possession",
                columns: table => new
                {
                    StockPossessionID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    BuyDate = table.Column<DateTime>(nullable: false),
                    BuyPrice = table.Column<float>(nullable: false),
                    AllTimeHigh = table.Column<float>(nullable: false),
                    AllTimeLow = table.Column<float>(nullable: false),
                    StockId = table.Column<int>(nullable: true),
                    OwnerGuid = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Possession", x => x.StockPossessionID);
                    table.ForeignKey(
                        name: "FK_Possession_UserModel_OwnerGuid",
                        column: x => x.OwnerGuid,
                        principalTable: "UserModel",
                        principalColumn: "Guid",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Possession_Stock_StockId",
                        column: x => x.StockId,
                        principalTable: "Stock",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Possession_OwnerGuid",
                table: "Possession",
                column: "OwnerGuid");

            migrationBuilder.CreateIndex(
                name: "IX_Possession_StockId",
                table: "Possession",
                column: "StockId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Possession");

            migrationBuilder.DropTable(
                name: "UserModel");

            migrationBuilder.DropTable(
                name: "Stock");
        }
    }
}

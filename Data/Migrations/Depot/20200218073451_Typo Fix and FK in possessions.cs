﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace StonksTracker.Migrations.Depot
{
    public partial class TypoFixandFKinpossessions : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Possession_UserModel_OwnerGuid",
                table: "Possession");

            migrationBuilder.DropForeignKey(
                name: "FK_Possession_Stock_StockId",
                table: "Possession");

            migrationBuilder.AlterColumn<int>(
                name: "StockId",
                table: "Possession",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "OwnerGuid",
                table: "Possession",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(450)",
                oldNullable: true);

            migrationBuilder.AddColumn<int>(
                name: "UserModelId",
                table: "Possession",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddForeignKey(
                name: "FK_Possession_UserModel_OwnerGuid",
                table: "Possession",
                column: "OwnerGuid",
                principalTable: "UserModel",
                principalColumn: "Guid",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Possession_Stock_StockId",
                table: "Possession",
                column: "StockId",
                principalTable: "Stock",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Possession_UserModel_OwnerGuid",
                table: "Possession");

            migrationBuilder.DropForeignKey(
                name: "FK_Possession_Stock_StockId",
                table: "Possession");

            migrationBuilder.DropColumn(
                name: "UserModelId",
                table: "Possession");

            migrationBuilder.AlterColumn<int>(
                name: "StockId",
                table: "Possession",
                type: "int",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<string>(
                name: "OwnerGuid",
                table: "Possession",
                type: "nvarchar(450)",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AddForeignKey(
                name: "FK_Possession_UserModel_OwnerGuid",
                table: "Possession",
                column: "OwnerGuid",
                principalTable: "UserModel",
                principalColumn: "Guid",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Possession_Stock_StockId",
                table: "Possession",
                column: "StockId",
                principalTable: "Stock",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}

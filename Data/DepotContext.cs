﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using StonksTracker.Models.Stocks;

namespace StonksTracker.Data
{
    public class DepotContext : DbContext
    {
        public DepotContext(DbContextOptions<DepotContext> options)
            : base(options)
        {
        }

        public DbSet<UserModel> UserModel { get; set; }
        public DbSet<StockPossession> Possession { get; set; }
        public DbSet<Stock> Stock { get; set; }
        public DbSet<PriceHistory> PriceHistory { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<UserModel>().ToTable("UserModel");
            modelBuilder.Entity<StockPossession>().ToTable("Possession");
            modelBuilder.Entity<Stock>().ToTable("Stock");
            modelBuilder.Entity<PriceHistory>().ToTable("PriceHistory");
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using StonksTracker.Data;
using StonksTracker.Models.Stocks;
using StonksTracker.Services.StockService;

namespace StonksTracker
{
    public class AddStockPossessionModel : PageModel
    {
        private readonly DepotContext _context;
        private readonly IBackgroundJob _backgroundJob;
        private readonly UserManager<IdentityUser> _userManager;

        // public IBackgroundJob _backgroundJob { get; }


        public AddStockPossessionModel(
            DepotContext context,
            BackgroundJob backgroundJob,
            UserManager<IdentityUser> userManager
            )
        {
            _context = context;
            _backgroundJob = backgroundJob;
            _userManager = userManager;

        }

        public IActionResult OnGet()
        {
            Console.WriteLine("Going to page");
            return Page();
        }

        [BindProperty]

        [Required]
        [Display(Name = "Internationale Kennnummer für Wertpapiere (ISIN)")]
        public string ISIN { get; set; }
        public DateTime BuyDate { get; set; }
        [Required]
        public float Price { get; set; }



        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.

        // ToDo: add antiforgery or somehow make this work..
        public async Task<IActionResult> OnPostAsync(string userId)
        {

            var user = await _userManager.GetUserAsync(HttpContext.User);

            if (!ModelState.IsValid)
            {
                return Page();
            }

            // Search the Stock by entered ISIN
            List<Stock> stocklist = await INGApi.searchStock(ISIN);
            Stock thisStock = stocklist[0];

            if (_backgroundJob.IsStockInDb(thisStock))
            {
                var a = "This stock is in!!!";
            }
            else
            {
                
                await _backgroundJob.AddStockToDb(thisStock);
                var a = "It is in now!!";
            }

            // Add the Stock to the users possesion


            string buyDate = Request.Form["BuyDate"];
            string buyPrice = Request.Form["BuyPrice"];

            StockPossession newPossession = new StockPossession();

            newPossession.Stock = thisStock;
            newPossession.Owner = _backgroundJob.GetUserById(user.Id);
            newPossession.BuyDate = DateTime.Parse(buyDate);
            newPossession.BuyPrice = float.Parse(buyPrice);

            _backgroundJob.AddStockPossession(newPossession);

            return RedirectToPage("./Index");
        }
    }
}

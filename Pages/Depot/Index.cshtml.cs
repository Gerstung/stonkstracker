﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using StonksTracker.Data;
using StonksTracker.Models.Stocks;

namespace StonksTracker
{
    public class IndexModel : PageModel
    {
        private readonly StonksTracker.Data.DepotContext _context;

        public IndexModel(StonksTracker.Data.DepotContext context)
        {
            _context = context;
        }

        public IList<StockPossession> StockPossession { get;set; }

        public async Task OnGetAsync()
        {
            StockPossession = await _context.Possession.ToListAsync();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace StonksTracker.Models.Sites
{
    public class KontaktModel
    {
        [EmailAddress]
        [Display(Name = "Email")]
        [Required]
        public string email { get; set; }


        [Display(Name = "Nachricht")]
        [Required]
        
        public string message { get; set; }
    }
}

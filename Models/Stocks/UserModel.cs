﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace StonksTracker.Models.Stocks
{
    // This Model is only used for the Depot
    public class UserModel
    {
        [Key]
        public string Guid { get; set; }
        public List<StockPossession> Possessions { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace StonksTracker.Models.Stocks
{
    public enum StockType
    {
        Index,  // Index
        Fonds,  // Fonds
        ETF,    // ETF
        Stock,  // Aktie
        Bond    // Anleihe
    }

    public enum Currency
    {
        EUR,
        USD
    }
    
    public class Stock
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Wkn { get; set; }
        public string Isin { get; set; }
        public StockType? Type { get; set; }
        public Currency? Currency { get; set; }
        public float Price { get; set; }
        public DateTime LastUpdate { get; set; }
        public Boolean StockOpen { get; set; }

        public List<StockPossession> Possessions { get; set; }

        public List<PriceHistory> PriceHistory { get; set; }



    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace StonksTracker.Models.Stocks
{
    public class StockPossession
    {
        [Key]
        public int StockPossessionID { get; set; }

        public int StockId { get; set; }
        public int UserModelId { get; set; }
        
        [Display(Name = "Kaufdatum")]
        public DateTime BuyDate { get; set; }
        [Display(Name = "Kaufpreis")]
        public float BuyPrice { get; set; }
        public float AllTimeHigh { get; set; }
        public float AllTimeLow { get; set; }

        public Stock Stock { get; set; }
        public UserModel Owner { get; set; }
    }
}

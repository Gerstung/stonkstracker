﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace StonksTracker.Models.Stocks
{
    public class PriceHistory
    {
        [Key]
        public int ID { get; set; }
        public Stock Stock { get; set; }
        public DateTime datetime { get; set; }
        public float price { get; set; }
    }
}

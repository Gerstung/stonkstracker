﻿using Microsoft.EntityFrameworkCore;
using StonksTracker.Data;
using StonksTracker.Models.Stocks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Timers;

namespace StonksTracker.Services.StockService
{
    public class BackgroundJob : IBackgroundJob
    {
        private DbContextOptions<DepotContext> _context;


        public BackgroundJob(DbContextOptions<DepotContext> dbContextOptions)
        {
            _context = dbContextOptions;
            ScheduleTimer(100000);
        }


        public async Task AddStockToDb (Stock stock)
        {
            using (var db = new DepotContext(_context))
            {
                db.Stock.Add(stock);
                await db.SaveChangesAsync();
            }
            return;
        }
        
        public async Task AddStockToDbIsin(string isin)
        {
            throw new NotImplementedException();
        }

        public async Task AddStockToDbWkn(string wkn)
        {
            throw new NotImplementedException();
        }

        public Boolean IsStockInDb(Stock stock)
        {
            if (!String.IsNullOrEmpty(stock.Isin))
            {
                return IsStockInDbIsin(stock.Isin);
            }
            else if (!String.IsNullOrEmpty(stock.Wkn))
            {
                return IsStockInDbWkn(stock.Wkn);
            }
            else
            {
                return false;
            }
        }

        public Boolean IsStockInDbIsin(string isin)
        {
            using (var db = new DepotContext(_context))
            {
                if (db.Stock.Count(s => s.Isin.Equals(isin)) != 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public Boolean IsStockInDbWkn(string wkn)
        {
            using (var db = new DepotContext(_context))
            {
                if (db.Stock.Count(s => s.Wkn.Equals(wkn)) != 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public void AddStockPossession(StockPossession possession)
        {
            using (var db = new DepotContext(_context))
            {
                var stock = db.Stock.Include(s => s.Possessions).First();
                db.Possession.Add(possession);
                db.SaveChanges();
                return;
            }
        }

        public UserModel GetUserById(string id)
        {
            using (var db = new DepotContext(_context))
            {
                return db.UserModel.FirstOrDefault(i => i.Guid.Equals(id));
            }
        }

        private void ScheduleTimer(int intervall)
        {            
            var aTimer = new Timer(60 * intervall * 1000); //one hour in milliseconds
            aTimer.Elapsed += new ElapsedEventHandler(OnTimedEvent);
            aTimer.Start();
        }
        private void OnTimedEvent(object source, ElapsedEventArgs e)
        {
            using (var db = new DepotContext(_context))
            {
                db.Stock.ForEachAsync(async stock  =>
                {
                    float newPrice;
                    DateTime newDate;
                    (newDate, newPrice) = await INGApi.getStockPriceIsin(stock.Isin);

                    stock.LastUpdate = newDate;
                    stock.Price = newPrice;

                    PriceHistory newEntry = new PriceHistory();
                    newEntry.Stock = stock;
                    newEntry.price = newPrice;
                    newEntry.datetime = newDate;

                    db.PriceHistory.Add(newEntry);
                    await db.SaveChangesAsync();
                });
            }
        }
    }
}

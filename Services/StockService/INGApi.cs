﻿using StonksTracker.Models.Stocks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text.Json;
using System.Threading.Tasks;

namespace StonksTracker.Services.StockService

// Maybe use this: https://documentation.tradier.com/brokerage-api/markets/get-quotes
{
    public enum TimeRange
    {
        OneWeek,
        OneMonth,
        OneYear,
        ThreeYears,
        Maximum
    }

    public class INGApi
    {
        const string SEARCH_URL = "https://api.wertpapiere.ing.de/suche-autocomplete/autocomplete?query=";
        const string PRICE_URL = "https://component-api.wertpapiere.ing.de/api/v1/components/instrumentheader/";
        const string CHART_URL = "https://component-api.wertpapiere.ing.de/api/v1/charts/shm/";


        private static readonly HttpClient client = new HttpClient();
       
        
        // This function is only working with the ING Rest api
        public static async Task<List<Stock>> searchStock(string searchTerm)
        {
            string completeUrl = SEARCH_URL + searchTerm;
            List<Stock> stocks = new List<Stock>();
            try
            {
                for(int retries = 0; retries < 3; retries++)
                {
                    var body = await client.GetStringAsync(completeUrl);
                    using var jsonDoc = JsonDocument.Parse(body);
                    var root = jsonDoc.RootElement;

                    int total = root.GetProperty("total").GetInt32();

                    if (total == 0)
                    {
                        await Task.Delay(1000);  // Wait 1s and try again
                        continue;
                    }

                    var stockList = root.GetProperty("suggestion_types")[0].GetProperty("suggestion_groups");

                    for (int i = 0; i < stockList.GetArrayLength(); i++)
                    {
                        var type = stockList[i].GetProperty("group").GetString();
                        if (type == "wp")    // Only search through Wertpapiere
                        {
                            var innerList = stockList[i].GetProperty("suggestions");
                            for (int j = 0; j < innerList.GetArrayLength(); j++)
                            {
                                Stock singleStock = new Stock();
                                singleStock.Isin = innerList[j].GetProperty("isin").GetString();
                                singleStock.Wkn = innerList[j].GetProperty("wkn").GetString();
                                singleStock.Name = innerList[j].GetProperty("text").GetString();

                                if(innerList[j].TryGetProperty("price", out JsonElement price))
                                {
                                    singleStock.Price = (float)price.GetDouble();
                                }

                                if (innerList[j].TryGetProperty("category", out JsonElement category))
                                {
                                    StockType stockCategory;
                                    switch (category.GetString())
                                    {
                                        case "ETFs": stockCategory = StockType.ETF; break;
                                        case "Aktien": stockCategory = StockType.Stock; break;
                                        case "Indizes": stockCategory = StockType.Index; break;
                                        default: stockCategory = StockType.Stock; break;
                                    }
                                    singleStock.Type = stockCategory;
                                }

                                
                                

                                stocks.Add(singleStock);
                            }
                        }
                    }

                    return stocks;
                }

                return null;
            }

            catch (Exception e)
            {
                Console.WriteLine(e);
                throw new Exception();
            }

            

            // ToDo Parse the JSON and return new stock object
            // Maybe return list of Stock Objects

            throw new NotImplementedException(); 
        }

        public static async Task<(DateTime, float)> getStockPriceIsin(string isin)
        {
            if (String.IsNullOrWhiteSpace(isin)) throw new System.ArgumentException("Parameter cannot be null or empty", "isin");

            float price;
            DateTime timestamp;

            try
            {
                string completeUrl = PRICE_URL + isin;

                var body = await client.GetStringAsync(completeUrl);
                using var jsonDoc = JsonDocument.Parse(body);
                var root = jsonDoc.RootElement;

                price = (float)root.GetProperty("price").GetDouble();
                string rawTimestamp = root.GetProperty("priceChangeDate").GetString();
                timestamp = DateTime.Parse(rawTimestamp);

                return (timestamp, price);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw new Exception();
            }
        }

        public static async Task<(List<DateTime>, List<float>)> getStockChartIsin(string isin, TimeRange timeRange)
        {
            if (String.IsNullOrWhiteSpace(isin)) throw new System.ArgumentException("Parameter cannot be null or empty", "isin");

            List<DateTime> dateTimeList = new List<DateTime>();
            List<float> dataList = new List<float>();

            try
            {
                // URL looks like this: base url + isin + query parameter timeRange=""&exchangeId=""&currencyId=""
                string completeUrl = CHART_URL + isin + "?timeRange=" + timeRange.ToString();

                var body = await client.GetStringAsync(completeUrl);
                using var jsonDoc = JsonDocument.Parse(body);
                var root = jsonDoc.RootElement;

                // Nested if to safely get instruments.0.data JSON object
                // ToDo: Optimize this ugly piece of c...ode
                if (root.TryGetProperty("instruments", out JsonElement instruments))
                {
                    if (instruments[0].TryGetProperty("data", out JsonElement dataSeries))
                    {
                        for (int i = 0; i < dataSeries.GetArrayLength(); i++)
                        {
                            long rawTimestamp = (long) dataSeries[i][0].GetDouble();

                            DateTime timestamp = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
                            timestamp = timestamp.AddMilliseconds(rawTimestamp).ToLocalTime();


                            float rawData = (float) dataSeries[i][1].GetDouble();

                            dateTimeList.Add(timestamp);
                            dataList.Add(rawData);
                        }
                    }
                }



                return (dateTimeList, dataList);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw new Exception();
            }
        }
    }
}

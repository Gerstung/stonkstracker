﻿using StonksTracker.Data;
using StonksTracker.Models.Stocks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StonksTracker.Services.StockService
{
    public interface IBackgroundJob
    {
        Task AddStockToDb(Stock stock);
        Task AddStockToDbWkn(string wkn);
        Task AddStockToDbIsin(string isin);
        Boolean IsStockInDb(Stock stock);

        Boolean IsStockInDbWkn(string wkn);
        Boolean IsStockInDbIsin(string isin);

        void AddStockPossession(StockPossession possession);
        UserModel GetUserById(string id);


    }
}
